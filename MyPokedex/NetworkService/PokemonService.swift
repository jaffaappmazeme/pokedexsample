//
//  PokemonService.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import Foundation
import Combine
import SwiftUI

public enum ServicingError: Error {
case image(error: Error)
case imageData(error: Error)
}

public protocol Servicing {
    func fetchImageWith(url: URL) -> AnyPublisher<Data, ServicingError>
    func fetchPokemonList(next: String?) -> AnyPublisher<PokemonListResponse, ServicingError>
    func fetchPokemonDetails(url: URL) -> AnyPublisher<PokemonDetails, ServicingError>
}

public class PokemonService: Servicing {
    let backgroundThread = DispatchQueue.global()
    var decoder = JSONDecoder()
    
    public init() {
        decoder.keyDecodingStrategy = .convertFromSnakeCase
    }
    
    public func fetchImageWith(url: URL) -> AnyPublisher<Data, ServicingError> {
        let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10.0)
        
        let fetchRequest = CacheItem.fetchRequest()
        fetchRequest.fetchLimit = 1
        
        fetchRequest.predicate = NSPredicate(
            format: "id=%@", url.absoluteString
        )
        
        
        do {
            if let result = try PersistenceController.shared.container.viewContext.fetch(fetchRequest).first {
            
            return Just(result.data!)
            
                .mapError { ServicingError.image(error: $0) }.eraseToAnyPublisher()
            }
        } catch {
        }
        
        return URLSession.shared.dataTaskPublisher(for: request).subscribe(on: backgroundThread).map {
            if let response = $0.response as? HTTPURLResponse {
                if response.statusCode == 200 {
            do {
                let item = CacheItem(context: PersistenceController.shared.container.viewContext)
                item.data = $0.data
                item.id = url.absoluteString
                item.timestamp = Date()
                try PersistenceController.shared.container.viewContext.save()
            } catch { }
            }
            }
            return $0.data
            
        }
        .mapError {
            ServicingError.image(error: $0)
        }.eraseToAnyPublisher()
    }
    
    public func fetchPokemonList(next: String?) -> AnyPublisher<PokemonListResponse, ServicingError>  {
        let url = next ?? "https://pokeapi.co/api/v2/pokemon?limit=20000"
        let request = URLRequest(url: URL(string: url)!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10.0)
        
        let fetchRequest = CacheItem.fetchRequest()
        fetchRequest.fetchLimit = 1
        
        fetchRequest.predicate = NSPredicate(
            format: "id=%@", url
        )
        
        
        do {
            if let result = try PersistenceController.shared.container.viewContext.fetch(fetchRequest).first {
            
            return Just(result.data!).decode(type: PokemonListResponse.self, decoder: decoder)
            
                .mapError { ServicingError.image(error: $0) }.eraseToAnyPublisher()
            }
        } catch {
        }
        
        return URLSession.shared.dataTaskPublisher(for: request)
            .subscribe(on: backgroundThread)
            .tryMap {
                if let response = $0.response as? HTTPURLResponse {
                    if response.statusCode == 200 {
                do {
                    let item = CacheItem(context: PersistenceController.shared.container.viewContext)
                    item.data = $0.data
                    item.id = url
                    item.timestamp = Date()
                    try PersistenceController.shared.container.viewContext.save()
                } catch { }
                }
                }
                return $0.data
            }
            .decode(type: PokemonListResponse.self, decoder: decoder)
            .mapError { ServicingError.image(error: $0) }
            .eraseToAnyPublisher()
    }
    
    public func fetchPokemonDetails(url: URL) -> AnyPublisher<PokemonDetails, ServicingError> {
        let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10.0)
        
        let fetchRequest = CacheItem.fetchRequest()
        fetchRequest.fetchLimit = 1
        
        fetchRequest.predicate = NSPredicate(
            format: "id=%@", url.absoluteString
        )
        
        
        do {
            if let result = try PersistenceController.shared.container.viewContext.fetch(fetchRequest).first {
            
            return Just(result.data!).decode(type: PokemonDetails.self, decoder: decoder)
                .mapError { ServicingError.image(error: $0) }.eraseToAnyPublisher()
            }
        } catch {
        }
        
        return URLSession.shared.dataTaskPublisher(for: request)
            .subscribe(on: backgroundThread)
            .tryMap {
                if let response = $0.response as? HTTPURLResponse {
                    if response.statusCode == 200 {
                do {
                    let item = CacheItem(context: PersistenceController.shared.container.viewContext)
                    item.data = $0.data
                    item.id = url.absoluteString
                    item.timestamp = Date()
                    try PersistenceController.shared.container.viewContext.save()
                } catch { }
                }
                }
                return $0.data
            }
            .decode(type: PokemonDetails.self, decoder: decoder)
            .mapError {
                print($0)
                return ServicingError.image(error: $0)
                
            }
            .eraseToAnyPublisher()
    }
}
