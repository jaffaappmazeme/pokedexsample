//
//  Pokemon.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import Foundation

public struct PokemonListResponse: Decodable {
    let count: Int
    let next: String?
    let previoud: String?
    let results: [Pokemon]
}

public struct Pokemon: Decodable {
    public var name: String?
    public var url: String?
}

public struct PokemonDetails: Decodable {
    public var id: Int
    public var name: String
    public var weight: Int
    public var height: Int
    public var sprites: Sprite
    
    private enum CodingKeys: String, CodingKey {
        case id, name, weight, height, sprites
    }
}

public struct Sprite: Decodable {
    public var frontDefault: String
    
    private enum CodingKeys: String, CodingKey {
        case frontDefault = "frontDefault"
    }
}
