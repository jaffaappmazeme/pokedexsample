//
//  Resolver.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import Resolver
import Foundation

extension Resolver: ResolverRegistering {
    public static func registerAllServices() {
        main.register {
            PokemonService() as Servicing }.scope(.application)
        }
}
