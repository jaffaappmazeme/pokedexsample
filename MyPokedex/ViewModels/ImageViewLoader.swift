//
//  ImageViewLoader.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import Foundation
import OSLog
import Combine
import Resolver
import class UIKit.UIImage

class ImageViewLoader: ObservableObject {
    @LazyInjected var service: Servicing
    private var cache: ImageCache?
    
    private var cancellable: AnyCancellable?
    
    @Published var imageThumbnail: UIImage?
    @Published var image: PokemonImageModel
    @Published var error: String?
    
    init(image: PokemonImageModel, cache: ImageCache? = nil) {
        self.image = image
        self.cache = cache
    }
    
    deinit {
        cancel()
    }
    
    func loadThumbnail() {
        if self.imageThumbnail != nil {
            return
        }
        
        
        
        let url = URL(string: image.url)!
        
        if let image = cache?[url] {
            self.imageThumbnail = image
            self.objectWillChange.send()
            return
        }
        
        cancellable = service.fetchPokemonDetails(url: url)
            .compactMap { $0 }
            .receive(on: DispatchQueue.global())
            .sink(
                receiveCompletion: { [weak self] error in
                    switch error {
                    case .failure:
                    DispatchQueue.main.async {
                        self?.error = "Error Loading Image"
                    }
                        break
                    default:
                        break
                    }
                }, receiveValue: { [weak self] details in
                    let sprite_url = details.sprites.frontDefault
                    self?.cancellable = self?.service.fetchImageWith(url: URL(string: sprite_url)!)
                        .compactMap { UIImage(data: $0) }
                        .receive(on: RunLoop.main)
                        .sink(receiveCompletion: { error in
                            switch error {
                            case .failure:
                                self?.error = "Error Loading Image"
                                break
                            default:
                                break
                            }
                        },
                              receiveValue: { [weak self] in
                            self?.error = nil
                            self?.imageThumbnail = $0
                            self?.cache(url, $0)
                            self?.objectWillChange.send()
                        })
                
            })
            
    }
    
    func cancel() {
        cancellable?.cancel()
    }
    
    private func cache(_ url: URL, _ image: UIImage?) {
        image.map { cache?[url] = $0 }
    }
}
