//
//  PokemonListViewModel.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import Foundation
import Combine
import OSLog

import Resolver

class PokemonListViewModel: ObservableObject {
    @LazyInjected var service: Servicing
    
    private let kDebounceInterval = 250
    private var nextPage: String? = nil
    private var currentSearch: String? = nil
    private let backgroundThread = DispatchQueue.global()
    private var cancellables = Set<AnyCancellable>()
    
    @Published var isLoading = false
    @Published var errorMessage: String?
    @Published var searchText: String = ""
    @Published var isNextPageLoading = false
    @Published var pokemon = [PokemonImageModel]()
    @Published var searchedPokemon = [PokemonImageModel]()
    
    init() {

    }
    
    func isLastImage(_ model: PokemonImageModel) -> Bool {
        pokemon.last?.name == model.name
    }
    
    func fetchNextPage() {
        isNextPageLoading = true
        
        fetchPokemon()
            .sink { completion in
                if case .failure(let error) = completion {
                    self.errorMessage = error.localizedDescription
                }
                self.isNextPageLoading = false
                
            } receiveValue: { data in
                DispatchQueue.global().async {
                    var newPokemon = self.pokemon
                    newPokemon.append(contentsOf: data.results.map { PokemonImageModel(url: $0.url ?? "", name: $0.name ?? "") })
                    DispatchQueue.main.async {
                        self.pokemon = newPokemon
                        if self.searchText.isEmpty {
                            self.searchedPokemon = self.pokemon
                        } else {
                            self.searchedPokemon = self.pokemon.filter({ model in
                                return model.name.lowercased().contains(self.searchText.lowercased())
                            })
                        }
                        self.nextPage = data.next
                    }
                }
            }
            .store(in: &self.cancellables)
    }
    
    func search(_ search: String?) {
        DispatchQueue.main.async {
            if self.searchText.isEmpty {
                self.searchedPokemon = self.pokemon
            } else {
                self.searchedPokemon = self.pokemon.filter({ model in
                    return model.name.lowercased().contains(self.searchText.lowercased())
                })
            }
        }
    }
    
    private func fetchPokemon() -> AnyPublisher<PokemonListResponse, ServicingError> {
        return service.fetchPokemonList(next: nextPage)
            .subscribe(on: backgroundThread)
            .map { $0 }
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}
