//
//  PokemonImageModel.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import UIKit

struct PokemonImageModel {
    var id: UUID
    var url: String = ""
    var name: String = ""
    
    init(url: String, name: String) {
        self.url = url
        self.name = name
        self.id = UUID()
    }
}

extension PokemonImageModel: Hashable, Identifiable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public static func == (lhs: PokemonImageModel, rhs: PokemonImageModel) -> Bool {
        lhs.name == rhs.name
    }
}
