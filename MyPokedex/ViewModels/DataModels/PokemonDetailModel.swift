//
//  PokemonImageModel.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import UIKit

struct PokemonDetailModel {
    var id: Int
    var name: String
    var weight: Int
    var height: Int
    var frontDefaultImage: Data
    
    init(id: Int, name: String, weight: Int, height: Int, frontDefaultImage: Data) {
        self.id = id
        self.name = name
        self.height = height
        self.weight = weight
        self.frontDefaultImage = frontDefaultImage
    }
}

extension PokemonDetailModel: Hashable, Identifiable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public static func == (lhs: PokemonDetailModel, rhs: PokemonDetailModel) -> Bool {
        lhs.name == rhs.name
    }
}
