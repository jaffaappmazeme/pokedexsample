//
//  PokemonListViewModel.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import Foundation
import Combine
import OSLog

import Resolver

class PokemonDetailViewModel: ObservableObject {
    @LazyInjected var service: Servicing
    
    private let kDebounceInterval = 250
    private let backgroundThread = DispatchQueue.global()
    private var cancellable: AnyCancellable?
    
    var detailUrl: String
    @Published var pokemonDetail: PokemonDetailModel?
    @Published var isLoading = false
    @Published var error: String?
    @Published var imageError: String?
    
    init(detailUrl: String) {
        self.detailUrl = detailUrl
    }
    
    deinit {
        cancel();
    }
    
    func fetchDetails() {
        isLoading = true
        fetchPokemonDetails()
    }
    
    private func fetchPokemonDetails() {
        cancellable = service.fetchPokemonDetails(url: URL(string: self.detailUrl)!)
            .compactMap { $0 }
            .subscribe(on: backgroundThread)
            .sink(receiveCompletion: { [weak self] error in
                switch error {
                case .failure:
                    DispatchQueue.main.async {
                        self?.error = "Error Loading Details"
                    }
                    break
                default:
                    break
                }
            }, receiveValue: { [weak self] details in
                let url = URL(string: details.sprites.frontDefault)!
                self?.cancellable = self?.service.fetchImageWith(url: url)
                    .map { PokemonDetailModel(id: details.id , name: details.name , weight: details.weight , height: details.height , frontDefaultImage: $0) }
                    .receive(on: RunLoop.main)
                    .sink(receiveCompletion: { [weak self] error in
                        switch error {
                        case .failure:
                            self?.imageError = "Error Loading Image"
                            break
                        default:
                            break
                        }
                    }, receiveValue: { [weak self] result in
                        self?.imageError = nil
                        self?.error = nil
                        self?.pokemonDetail = result
                        self?.isLoading = false
                    })
            })
    }
    
    func cancel() {
        cancellable?.cancel()
    }
}
