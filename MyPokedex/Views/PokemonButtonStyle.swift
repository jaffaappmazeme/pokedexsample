//
//  PokemonButtonStyle.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import SwiftUI

struct PokemonButtonStyle: ButtonStyle {
    @Environment(\.colorScheme) var colorScheme
    
    func makeBody(configuration: Self.Configuration) -> some View {
        let pressedForegroundColor = colorScheme == .dark ? Color.white : Color.white.opacity(0.7)
        let unpressedForegroundColor = colorScheme == .dark ? Color.white : Color.clear
        
        let pressedBackgroundColor = colorScheme == .dark ? Color.black.opacity(0.7) : Color.white
        let unpressedBackgroundColor = colorScheme == .dark ? Color.black.opacity(0.7) : Color.white
        
        configuration.label
            .foregroundColor(configuration.isPressed ? pressedForegroundColor : unpressedForegroundColor)
            .background(configuration.isPressed ? pressedBackgroundColor : unpressedBackgroundColor)
            .scaleEffect(configuration.isPressed ? 0.95 : 1.0)
    }
    
}

