//
//  ImageView.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import SwiftUI

struct ImageView: View {
    @StateObject private var loader: ImageViewLoader
    
    var body: some View {
        imageView(image: loader.image)
            .onAppear {
                loader.loadThumbnail()
            }
    }
    
    init(image: PokemonImageModel, cache: ImageCache? = nil) {
        _loader = StateObject(wrappedValue: ImageViewLoader(image: image, cache: cache))
    }
    
    func imageView(image: PokemonImageModel) -> some View {
        if let error = loader.error {
            return AnyView(
                Text(error)
                    .frame(width: 120, height: 120, alignment: .center)
            )
        } else if let image = loader.imageThumbnail {
            return AnyView(
                Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .clipped()
                    .frame(width: 120, height: 120, alignment: .center)
            )
        } else {
            return AnyView(
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle())
            )
        }
    }
}
