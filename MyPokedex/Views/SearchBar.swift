//
//  SearchBar.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import SwiftUI

struct SearchBar : View {
    @Binding var searchText: String
    
    var body: some View {
        HStack(alignment: .center, spacing: 0) {
            Image(systemName: "magnifyingglass")
                .padding(.leading, -10)
                .foregroundColor(.secondary)
            
            TextField("Search", text: $searchText, onEditingChanged: { blah in
                var y = 0
                y += 1;
            }, onCommit: {
                UIApplication.shared.windows.first { $0.isKeyWindow }?.endEditing(true)
            }).disableAutocorrection(true)
                .padding(.leading, 10)
            
            Button(action: {
                self.searchText = ""
            }) {
                Image(systemName: "xmark.circle.fill")
                    .foregroundColor(.secondary)
                    .opacity(searchText == "" ? 0 : 1)
            }
        }
        .padding()
        .overlay(RoundedRectangle(cornerRadius: 8)
                    .stroke(Color.blue.opacity(0.3), lineWidth: 2))
    }
}
