//
//  PokemonCardView.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import SwiftUI

struct PokemonCardView: View {
    @EnvironmentObject var viewModel: PokemonListViewModel
    @Environment(\.imageCache) var cache: ImageCache
    
    let model: PokemonImageModel
    
    var body: some View {
        ZStack {
            cardView(model: model)
        }
    }
    
    init(model: PokemonImageModel) {
        self.model = model
    }
    
    func cardView(model: PokemonImageModel) -> some View {
        return ZStack(alignment: .center) {
            RoundedRectangle(cornerRadius: 4, style: .continuous)
                .fill(Color.white)
                .shadow(color: Color.white, radius: 2, x: 2, y: 2)
                .blur(radius: 1, opaque: false)
            
            VStack(alignment: .center, spacing: 0) {
                ImageView(image: model, cache: cache)
                    .padding(.bottom)
                
                Text(model.name)
                    .font(.caption2)
                    .foregroundColor(Color.black)
                    .multilineTextAlignment(.leading)
                    .lineLimit(3)
                    .padding(8)
                
                if viewModel.isLastImage(model) {
                    Rectangle()
                        .frame(width: 1, height: 2)
                        .onAppear {
                            viewModel.fetchNextPage()
                        }
                }
            }
        }
    }
}
