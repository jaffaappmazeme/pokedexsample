//
//  PokemonDetailsView.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/30/21.
//

import SwiftUI

struct PokemonDetailsView: View {
    @Environment(\.verticalSizeClass) var verticalSizeClass: UserInterfaceSizeClass?
    @Environment(\.horizontalSizeClass) var horizontalSizeClass: UserInterfaceSizeClass?
    @Environment(\.presentationMode) var presentation
    @Environment(\.imageCache) var cache: ImageCache
    
    @ObservedObject var viewModel: PokemonDetailViewModel
    
      var body: some View {
          detailView().onAppear {
              viewModel.fetchDetails()
          }
      }
    
    func detailView() -> some View {
        if viewModel.error != nil || viewModel.imageError != nil {
            return AnyView(ScrollView {Text("Failed to load from network or cache. Check to see your Internet connection is active. After first run it will cache this page.").padding(20)})
        } else if viewModel.isLoading || viewModel.pokemonDetail == nil {
            return AnyView(
                ProgressView()
                )
        } else {
            if horizontalSizeClass == .compact && verticalSizeClass == .regular {
                return AnyView(
                    VStack(alignment: .leading, spacing: 0) {
                        Image(uiImage: UIImage(data: viewModel.pokemonDetail!.frontDefaultImage)!).resizable().frame(width: 360, height: 360, alignment: .center)
                        Text("Name:   \(viewModel.pokemonDetail?.name ?? "")")
                        Text("Weight: \(viewModel.pokemonDetail?.weight ?? 0)")
                        Text("Height: \(viewModel.pokemonDetail?.height ?? 0)")
                    }
                )
            } else {
                return AnyView(
                    HStack() {
                        Image(uiImage: UIImage(data: viewModel.pokemonDetail!.frontDefaultImage)!).resizable().frame(width: 360, height: 360, alignment: .center)
                        VStack(alignment: .leading, spacing: 0) {
                            Text("Name:   \(viewModel.pokemonDetail?.name ?? "")")
                            Text("Weight: \(viewModel.pokemonDetail?.weight ?? 0)")
                            Text("Height: \(viewModel.pokemonDetail?.height ?? 0)")
                        }
                        
                    }
                )
            }
            
            
        }
    }
}


