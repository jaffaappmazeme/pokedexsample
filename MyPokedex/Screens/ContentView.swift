//
//  ContentView.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/25/21.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.colorScheme) var colorScheme
    @Environment(\.imageCache) var cache: ImageCache
    
    @EnvironmentObject var viewModel: PokemonListViewModel
    @State private var selection: String?
    
    private var gridItemLayout = [GridItem(.adaptive(minimum: 120))]
    
    var body: some View {
        NavigationView {
            VStack {
                SearchBar(searchText: $viewModel.searchText)
                    .onChange(of: viewModel.searchText, perform: {
                        self.viewModel.search($0)
                    })
                    .padding()
                
                content.navigationTitle("Pokemon")
            }
        }.onAppear {
            viewModel.fetchNextPage()
        }
    }
    
    var content: some View {
        if let _ = viewModel.errorMessage {
            return AnyView(ScrollView {Text("Failed to load from network or cache. Check to see your Internet connection is active. After first run it will cache this page.").padding(20)})
        } else {

        return AnyView(ScrollView {

            LazyVGrid(columns: gridItemLayout, spacing: 20
            ) {
                ForEach(viewModel.searchedPokemon) { pokemon in
                    VStack(spacing: 0) {
                        NavigationLink(destination: PokemonDetailsView(viewModel: PokemonDetailViewModel(detailUrl: pokemon.url)), label: {
                            PokemonCardView(model: pokemon)
                        })

                    }
                }
            }
            .id(1)
            .padding()
            .background(GeometryReader {
                Color.clear.preference(key: ViewOffsetKey.self,
                                       value: -$0.frame(in: .named("scroll")).origin.y)
            })

            if viewModel.isNextPageLoading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle())
            }
        }
        .coordinateSpace(name: "scroll"))
        }
//        .navigationBarTitle(Text("Pokedex"), displayMode: .inline)
    }
    
}

struct ViewOffsetKey: PreferenceKey {
    typealias Value = CGFloat
    static var defaultValue = CGFloat.zero
    static func reduce(value: inout Value, nextValue: () -> Value) {
        value += nextValue()
    }
}
