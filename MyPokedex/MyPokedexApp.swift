//
//  MyPokedexApp.swift
//  MyPokedex
//
//  Created by Matt Jaffa on 11/25/21.
//

import SwiftUI
import Resolver
import CoreData

@main
struct MyPokedexApp: App {
    
//    let persistenceController = PersistenceController.shared
    
    init() {
        Resolver.registerAllServices()
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(PokemonListViewModel())
        }
    }
}
